import {createAction, createReducer} from "redux-act";

export const set = createAction('Set User Data');


const Auth = createReducer({
    [set]: (state, payload) => ({...state, ...payload})
});



export default Auth