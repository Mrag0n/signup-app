import React from 'react';
import ReactDOM from 'react-dom';
import {applyMiddleware, compose, createStore} from 'redux'
import {Provider} from 'react-redux'
import thunk from 'redux-thunk'
import MainScreen from './containers/MainScreen'
import Auth from "./reducers";


const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
const Store = createStore(Auth, composeEnhancers(applyMiddleware(thunk)));

ReactDOM.render(<Provider store={Store}><MainScreen/></Provider>, document.getElementById('root'));

