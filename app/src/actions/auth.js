import {set} from '../reducers/index'

export const setUserData = (data) => dispatch => {
    return dispatch(set(data));
};