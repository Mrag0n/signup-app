import {connect} from 'react-redux'
import MainScreen from '../components/MainScreen'
import {setUserData} from '../actions/auth'

export default connect(
    state => ({
        userData: state
    }),
    dispatch => ({
        setUserData: (data) => dispatch(setUserData(data))
    })
)(MainScreen)
