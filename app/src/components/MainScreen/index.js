import React from 'react'
import moment from 'moment'
import {OPTIONS} from '../../constants/index'
import {Dropdown} from 'semantic-ui-react'
import {
    AdditionalInfoContainer,
    AdditionalInfoTitle,
    ArrowDown,
    ArrowIcon,
    AuthorizationInfo,
    BackButton,
    CompleteContainer,
    CompleteIcon,
    DashboardButton,
    DateInput,
    DateOfBirthContainer,
    ErrorMsg,
    Footer,
    GenderButton,
    GenderContainer,
    Head,
    HeadText,
    Input,
    InputContainer,
    InputTitle,
    MainContent,
    NextButton,
    Progress,
    Wrapper
} from './styles'

class MainScreen extends React.Component {
    state = {
        userData: {
            email: '',
            password: '',
            confirmPassword: '',
            dateOfBirth: {
                day: '',
                month: '',
                year: ''
            },
            gender: '',
            heardAboutAs: null,
        },
        errors: {},
        errorsMsg: null,
        progress: {
            stage: 1,
            width: '33.3%'
        }
    };

    handleChange = name => e => {
        return this.setState({userData: {...this.state.userData, [name]: e.target.value}})
    };

    handleSubmitFirstStage = e => {
        e.preventDefault();
        let errors = {};
        let userData = this.state.userData;
        this.setState({errors, errorsMsg: null});
        let reg = new RegExp('^[a-zA-Z0-9.!#$%&\'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$');

        for (let v in userData) {
            if ((v === 'email' || v === 'password' || v === 'confirmPassword') && userData[v] === '') {
                errors = Object.assign({}, errors, {[v]: true});
                this.setState({errors, errorsMsg: `This field is required!`});
            }
        }

        if (!reg.test(userData.email))
            return this.setState({
                errors: {email: true},
                errorsMsg: `Type a valid email`
            });

        if (userData.password.length < 6)
            return this.setState({
                errors: {password: true},
                errorsMsg: `Minimum 6 characters`
            });

        if (userData.password !== userData.confirmPassword)
            return this.setState({
                errors: {confirmPassword: true},
                errorsMsg: `Should match the password.`
            });

        if (Object.keys(errors).length > 0) return null;

        this.props.setUserData({
            email: userData.email,
            password: userData.password,
            confirmPassword: userData.confirmPassword
        });
        return this.setState({
            progress: {
                stage: 2, width: '66.6%'
            }
        })
    };

    handleSubmitSecondStage = e => {
        e.preventDefault();
        let errors = {};
        let userData = this.state.userData;
        this.setState({errors, errorsMsg: null});
        let year = userData.dateOfBirth.year;
        let month = userData.dateOfBirth.month;
        let day = userData.dateOfBirth.day;
        let currentDate = moment();
        let date = moment([year, month, day]);

        for (let v in userData.dateOfBirth) {
            if ((v === 'day' || v === 'month' || v === 'year') && userData.dateOfBirth[v] === '') {
                errors = {dateOfBirth: true};
                this.setState({errors, errorsMsg: 'This fields is required!'});
            }
        }

        if (userData.gender === '') {
            errors = {gender: true};
            this.setState({errors, errorsMsg: 'One should be selected.'});
        }

        if (currentDate.diff(date, 'years') < 18)
            this.setState({errors: {dateOfBirth: true}, errorsMsg: 'You must be 18 year old'});

        if (Object.keys(errors).length > 0) return null;


        this.props.setUserData({
            gender: userData.gender,
            dateOfBirth: moment(date).format('DD/MM/YYYY'),
            heardAboutAs: userData.heardAboutAs !== '' ? userData.heardAboutAs : null
        });
        return this.setState({
            progress: {
                stage: 3, width: '100%'
            }
        })

    };

    handleChangeDay = event => {
        event.preventDefault();
        if (event.target.value.length > 2) return false;
        if (event.target.value.length > 0 && ['0', '1', '2', '3'].indexOf(event.target.value[0]) === -1) return false;
        if (event.target.value.length > 1 && event.target.value[0] === '0' && event.target.value[1] === '0') return false;
        if (event.target.value.length > 1 && event.target.value[1] === '.') return false;
        let value = parseInt(event.target.value);

        if (value > 31 || value < 0)
            return false;

        if (event.target.value !== '' && (isNaN(value) || isNaN(event.target.value)))
            return false;

        return this.setState({
            userData: {
                ...this.state.userData,
                dateOfBirth: {...this.state.userData.dateOfBirth, day: event.target.value}
            }
        });
    };

    handleChangeMonth = event => {
        event.preventDefault();
        if (event.target.value.length > 2) return false;
        if (event.target.value.length > 0 && ['0', '1'].indexOf(event.target.value[0]) === -1) return false;
        if (event.target.value.length > 1 && event.target.value[0] === '0' && event.target.value[1] === '0') return false;
        if (event.target.value.length > 1 && event.target.value[1] === '.') return false;

        let value = parseInt(event.target.value);
        if (value > 12 || value < 0)
            return false;

        if (event.target.value !== '' && (isNaN(value) || isNaN(event.target.value)))
            return false;

        return this.setState({
            userData: {
                ...this.state.userData,
                dateOfBirth: {...this.state.userData.dateOfBirth, month: event.target.value}
            }
        });
    };

    handleChangeYear = event => {
        event.preventDefault();
        if (event.target.value.length > 4) return false;
        if (event.target.value.length > 0 && event.target.value[0] === '0') return false;
        if (event.target.value.length > 1 && (event.target.value[1] === '.' || event.target.value[2] === '.' || event.target.value[3] === '.')) return false;
        let value = parseInt(event.target.value);

        if (value < 0)
            return false;

        if (event.target.value !== '' && (isNaN(value) || isNaN(event.target.value)))
            return false;

        return this.setState({
            userData: {
                ...this.state.userData,
                dateOfBirth: {...this.state.userData.dateOfBirth, year: event.target.value}
            }
        })
    };

    changeGender = (gender) => () => {
        return this.setState({userData: {...this.state.userData, gender}})
    };

    onSelect = (event, data) => {
        return this.setState({userData: {...this.state.userData, heardAboutAs: data.value}})
    };

    goBack = () => {
        return this.setState({progress: {stage: 1, width: '33.3%'}})
    };

    complete = () => {
        const userData = {userData: this.props.userData};
        console.log(JSON.stringify(userData));
    };


    render() {
        const stage = this.state.progress.stage;
        return <Wrapper>
            <MainContent
                onSubmit={stage === 1 ? this.handleSubmitFirstStage : this.handleSubmitSecondStage}>
                <Head><HeadText>{stage === 3 ? 'Thank You!' : 'Signup'}</HeadText></Head>
                <Progress width={this.state.progress.width}/>
                {stage === 1 && <AuthorizationInfo>
                    <InputContainer>
                        {this.state.errors.email ? <ErrorMsg>{this.state.errorsMsg}</ErrorMsg> :
                            <InputTitle>EMAIL</InputTitle>}
                        <Input type='text' onChange={this.handleChange('email')}
                               value={this.state.userData.email}/>
                    </InputContainer>
                    <InputContainer>
                        {this.state.errors.password ? <ErrorMsg>{this.state.errorsMsg}</ErrorMsg> :
                            <InputTitle>PASSWORD</InputTitle>}
                        <Input type='password' onChange={this.handleChange('password')}
                               value={this.state.userData.password}/>
                    </InputContainer>
                    <InputContainer>
                        {this.state.errors.confirmPassword ? <ErrorMsg>{this.state.errorsMsg}</ErrorMsg> :
                            <InputTitle>CONFIRM PASSWORD</InputTitle>}
                        <Input type='password' onChange={this.handleChange('confirmPassword')}
                               value={this.state.userData.confirmPassword}/>
                    </InputContainer>
                </AuthorizationInfo>}
                {stage === 2 && <AuthorizationInfo>
                    <AdditionalInfoContainer>
                        {this.state.errors.dateOfBirth ? <ErrorMsg>{this.state.errorsMsg}</ErrorMsg> :
                            <AdditionalInfoTitle>DATE OF BIRTH</AdditionalInfoTitle>}
                        <DateOfBirthContainer>
                            <DateInput onChange={this.handleChangeDay} value={this.state.userData.dateOfBirth.day}
                                       placeholder='DD'/>
                            <DateInput onChange={this.handleChangeMonth} value={this.state.userData.dateOfBirth.month}
                                       placeholder='MM'/>
                            <DateInput onChange={this.handleChangeYear} value={this.state.userData.dateOfBirth.year}
                                       placeholder='YYYY'/>
                        </DateOfBirthContainer>
                    </AdditionalInfoContainer>
                    <AdditionalInfoContainer>
                        {this.state.errors.gender ? <ErrorMsg>{this.state.errorsMsg}</ErrorMsg> :
                            <AdditionalInfoTitle>GENDER</AdditionalInfoTitle>}
                        <GenderContainer>
                            <GenderButton type='button' active={this.state.userData.gender === 'male'}
                                          onClick={this.changeGender('male')}>MALE</GenderButton>
                            <GenderButton type='button' active={this.state.userData.gender === 'female'}
                                          onClick={this.changeGender('female')}>FEMALE</GenderButton>
                            <GenderButton type='button' active={this.state.userData.gender === 'unspecified'}
                                          onClick={this.changeGender('unspecified')}>UNSPECIFIED</GenderButton>
                        </GenderContainer>
                    </AdditionalInfoContainer>
                    <AdditionalInfoContainer>
                        <AdditionalInfoTitle>WHERE DID YOU HEAR ABOUT US?</AdditionalInfoTitle>
                        <Dropdown onChange={this.onSelect} options={OPTIONS} fluid selection icon={<ArrowDown/>}/>
                    </AdditionalInfoContainer>
                </AuthorizationInfo>}
                {stage === 3 && <CompleteContainer>
                    <CompleteIcon/>
                    <DashboardButton type='button' onClick={this.complete}>Go to Dashboard <ArrowIcon/></DashboardButton>
                </CompleteContainer>}
                {stage !== 3 && <Footer withBack={stage === 2}>
                    {stage === 2 && <BackButton onClick={this.goBack}>Back</BackButton>}
                    <NextButton type='submit'>Next <ArrowIcon/></NextButton>
                </Footer>}
            </MainContent>
        </Wrapper>
    }
}


export default MainScreen