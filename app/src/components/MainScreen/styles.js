import styled from 'styled-components'
import Arrow from 'react-icons/lib/io/android-arrow-forward'
import ArrowDownIcon from 'react-icons/lib/fa/angle-down'
import Complete from 'react-icons/lib/md/check-circle'

export const Wrapper = styled.div`
    display: flex;
    justify-content: center;
    align-items: center;
    width: 100%;
    height: 100%;
    font-family: 'Poppins', sans-serif;
`;

export const MainContent = styled.form`
   height: 620px;
   width: 420px;
   border: 2px solid #cacaca;
   border-radius: 6px;
   position: relative;
`;

export const Head = styled.div`
   width: 100%;
   height: 70px;
   display: flex;
   justify-content: center;
   align-items: center;
   border: 0 solid #cacaca;
   border-bottom-width: 2px;
`;

export const HeadText = styled.p`
    color:#4B8FE2;
    font-size: 24px;
`;

export const AuthorizationInfo = styled.div`
    margin-top: 60px;
    padding-right: 20px;
    padding-left: 20px;
`;

export const InputContainer = styled.div`
    width: 100%;
    height: 80px;
    flex-direction: column;
    border: 0 solid #cacaca;
    border-bottom-width: 2px;
    margin-bottom: 20px;
`;

export const InputTitle = styled.p`
    font-size: 20px;
    color: #a7a7a7;
`;

export const Input = styled.input`
    width: 100%;
    border: none;
    outline:0;
    font-size: 16px;
    color: #a7a7a7;
`;

export const Footer = styled.div`
    position: absolute;
    bottom: 0;
    display: flex;
    align-items: center;
    justify-content:${props => props.withBack ? 'space-between' : 'flex-end'} ;
    width: 100%;
    height: 70px;
    border: 0 solid #cacaca;
    border-top-width: 2px;
`;

export const NextButton = styled.button`
    border: none;
    outline:0;
    background: #fff;
    color:#4B8FE2;
    font-size: 18px;
`;

export const BackButton = styled.button`
    border: none;
    outline:0;
    background: #fff;
    color:#7a7a7a;
    font-size: 18px;
`;

export const ArrowIcon = styled(Arrow)`
    color:#4B8FE2;
    font-size: 19px;
`;

export const Progress = styled.div`
    height: 10px;
    background: #4B8FE2;
    width: ${props => props.width};
`;

export const ErrorMsg = styled.p`
    font-size: 16px;
    color:red;
`;

export const AdditionalInfoContainer = styled.div`
    display: flex;
    margin-bottom: 30px;
    flex-direction: column;
    justify-content: center;
    align-items: center;
    width: 100%;
`;

export const AdditionalInfoTitle = styled.p`
    font-size: 20px;
    color: #a7a7a7;
`;

export const DateOfBirthContainer = styled.div`
    display: flex;
    flex-direction: row;
    width: 100%;
`;

export const DateInput = styled.input`
    outline: 0;
    height: 40px;
    width: 33.3%;
    text-align: center;
    font-size: 18px;
    color: #a7a7a7;
`;

export const GenderContainer = styled.div`
    display: flex;
    flex-direction: row;
    width: 100%;
`;

export const GenderButton = styled.button`
    width: 33.3%;
    height: 40px;
    font-size: 16px;
    color: ${props => props.active ? '#fff' : '#a7a7a7'};
    background: ${props => props.active ? '#4B8FE2' : '#fff'};
    border-radius: 4px;
    outline:0;
`;

export const ArrowDown = styled(ArrowDownIcon)`
    color:#4B8FE2;
    font-size: 20px;
    position: absolute;
    right: 5px;
`;

export const DashboardButton = styled.button`
    height: 40px;
    width: 150px;
    border: 1px solid #4B8FE2;
    border-radius: 4px;
    color: #4B8FE2;
    align-items: center;
    text-align: center;
    outline: 0;
`;

export const CompleteIcon = styled(Complete)`
    font-size: 230px;
    color:#b8e985;
`;

export const CompleteContainer = styled.div`
    display: flex;
    margin-top: 50px;
    justify-content: center;
    align-items: center;
    flex-direction: column;
`;